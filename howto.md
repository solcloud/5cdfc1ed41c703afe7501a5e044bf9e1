## FFmpeg record Xorg $DISPLAY screen

### CPU
`ffmpeg -probesize 18M -video_size 1920x1080 -framerate 30 -f x11grab -i $DISPLAY -c:v libx264rgb -crf 0 -preset ultrafast /dev/shm/output.mp4`

### GPU nvidia

`bindsym Mod1+Shift+i exec "killall ffmpeg"`

`sleep 1 && ffmpeg -probesize 18M -video_size 1920x1080 -framerate 30 -f x11grab -i $DISPLAY -c:v h264_nvenc -qp 0 /dev/shm/output.mp4`

Do not record mouse by adding `-draw_mouse 0`

## Re-encode

You can experiment with the -crf value to control output quality. Lower crf means better quality. A subjectively sane range is 18-28, where 18 is visually lossless or nearly so.
Use the slowest preset you have patience for: ultrafast, superfast, veryfast, faster, fast, medium, slow, slower, veryslow.

`ffmpeg -i /dev/shm/output.mp4 -c:v libx264 -crf 18 -preset medium /data/store/final.mp4`

## Remove audio

`ffmpeg -i input.mp4 -c copy -an output.mp4`

## Shrink video

`ffmpeg -i /dev/shm/output.mp4 -ss 00:00:05 -to 00:03:24 -c:v libx264 -crf 18 -preset medium -an upload.mp4`

